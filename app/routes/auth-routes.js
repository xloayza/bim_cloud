const router = require('express').Router();
const passport = require('passport');
const keys = require('../config/keys');
const User = require('../models/user-model');

const ForgeSDK = require('forge-apis');
const request = require('request');

// express and app are required for app.locals
const express = require('express');
const app = express();

// auth login
router.get('/login', function (req, res) {
    res.render('login.ejs', {
        pageTitle: 'Log In',
        pageID: 'login'
    })
});

router.post('/login', function (req, res) {
    console.log(req.body.email);
    var email = req.body.email;
    // app.locals to pass email to other route (/auth/pass)
    app.locals.email = email
    const autoRefresh = true;
    var oAuth2TwoLegged = new ForgeSDK.AuthClientTwoLegged(keys.forge.clientID, keys.forge.clientSecret, [
        'data:read',
        'data:write',
        'account:read',
        'account:write'
    ], autoRefresh);

    oAuth2TwoLegged.authenticate().then(function (credentials) {
        // GET authorization token
        const token = credentials.access_token;
        console.log('TwoLeggedToken: ' + token);
        //res.json({ access_token: credentials.access_token, expires_in: credentials.expires_in });
        // The `credentials` object contains an access_token that is being used to call the endpoints.
        // In addition, this object is applied globally on the oAuth2TwoLegged client that you should use when calling secure endpoints.
        //return token

        // GET users/search
        var options = {
            method: 'GET',
            url: 'https://developer.api.autodesk.com/hq/v1/accounts/' + keys.bim360.accountID + '/users/search',
            qs: { email: email },
            headers:
            {
                'Postman-Token': '2560c912-671e-4b56-be8e-99cccdb083f6',
                'cache-control': 'no-cache',
                Authorization: 'Bearer ' + token
            }
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            // JSON to object or array
            var userData = JSON.parse(body);
            console.log(userData[0]);
            try {
                // user exists on BIM360 Account?
                if (email == userData[0].email) {
                    // Is logging user on DB? 

                    if (email) {
                        console.log('user exists on DB');
                        res.redirect('/auth/pass');
                        // register new user
                    } else {
                        console.log('password registration');
                        res.redirect('/auth/pass-reg');
                    }
                    // user does not exist on BIM 360 Account    
                } else {
                    res.send(email + ' is not authorized to use this system')
                }
            }
            catch (error) {
                res.send(email + ' is not authorized to use this system')
            }
        });
    }, function (err) {
        console.error(err);
    });
});

// auth pass
router.get('/pass', function (req, res) {
    res.render('pass.ejs', {
        pageTitle: 'Log In',
        pageID: 'pass'
    });
});

router.post('/pass', passport.authenticate('local', { failureRedirect: '/login' }),
    function (req, res) {
        res.redirect('/');
    });

router.get('/pass-reg', function (req, res) {
    res.render('pass-reg.ejs', {
        pageTitle: 'Registration',
        pageID: 'registration'
    });
});

router.post('/pass-reg', function (req, res) {
    if (req.body.password === req.body.password_verify) {
        console.log('is same password')
        const username = app.locals.email
        const password = req.body.password
        User.create({ username, password })
            .then(user => {
                req.login(user, err => {
                    if (err) next(err);
                    else res.redirect("/");
                });
            })
            .catch(err => {
                if (err.name === "ValidationError") {
                    req.flash("Sorry, that username is already taken.");
                    res.redirect("/auth/pass-reg");
                } else next(err);
            });
    }
});

// auth logout
router.get('/logout', function (req, res) {
    res.send('logging out')
});

// auth with forge
router.get('/forge', function (req, res) {
    res.send('logging in with forge')
});

router.get('/forge/redirect', function (req, res) {
    res.send('forge redirect')
});


module.exports = router;