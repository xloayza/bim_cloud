const router = require('express').Router();

// index route
router.get('/', function (req, res) {
    res.render('index.ejs', {
        pageTitle: 'Home',
        pageID: 'home'
    })
});

// Logout Handler
router.all("/logout", function (req, res) {
    req.logout();
    res.redirect("/login");
});


module.exports = router;