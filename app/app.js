const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.set('port', process.env.PORT || 8000);
app.set('view engine', 'ejs');
app.set('views', 'app/views', () =>{
    console.log(views)
});


app.locals.siteTitle = 'AXObim';

app.use(express.static('app/public'));
// set up routes
app.use('/auth', require('./routes/auth-routes.js'));
app.use('/', require('./routes/index-routes.js'));

var server = app.listen(app.get('port'), function(){
    console.log('listening on port ' + app.get('port'))
});